// 9).Write a program for perform a bubble sort algorithm
function Sort(arr) {
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr.length - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}
var arr = [5, 0, 8, 20, 10, 50, 55];
console.log(Sort(arr));
