// 2).What is the output of below code ?
//1
let result;

result = "hello" - "world";
console.log(result); //NaN
// it converts both sides to numbers and gets Not A Number

//2
result = String(2 + 4);
console.log(result); //6
// 2+4=6 then 6 is converted to string

// 3
(function () {
  console.log(1);
  setTimeout(function () {
    console.log(2);
  }, 1000);
  setTimeout(function () {
    console.log(3);
  }, 0);
  console.log(4);
})(); //1-4-3-2

/**
 * conole.log are synchronous code. They execute in the sequence.
 * setTimeout is asynchronous funtions which are executed as per the duration defined in argumnts.
 */
