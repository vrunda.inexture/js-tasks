/**
 * 8)Remove repeated Element from array without using any loops and Array method
 * Input: let nums = [1, 2, 3, 5, 3, 2, 2, 4, 6]
 * Output: [1, 2, 3, 4, 5, 6]
 */

let nums = [1, 2, 3, 5, 3, 2, 2, 4, 6];
function unique(data) {
  return [...new Set(nums)];
}
console.log(unique(nums));
