// 3).Write a function for below definition

// 1 Here you need to write one common function which is give me output if I m calling below method

function sum(x, y) {
  if (y !== undefined) {
    return x + y;
  } else {
    return function (y) {
      return x + y;
    };
  }
}
console.log(sum(2, 3)); // output was 5
console.log(sum(2)(3)); // output was 5

// 2 : LONGEST WORD
// Return the longest word of a string ex.longestWord('Hi there, my name is Brad') === 'there,'
function longestWord(text) {
  let word = text.split(" ");
  var maxwordLength = 0;
  var longestWord = "";

  for (let i = 0; i < word.length; i++) {
    if (word[i].length > maxwordLength) {
      maxwordLength = word[i].length;
      longestWord = word[i];
    }
  }
  return longestWord.replace(",", "");
}
console.log(longestWord("hi there, my name is Brad"));

// 5).Write a program for below definition

// 1. VALIDATE A PALINDROME Return true if palindrome and false if not.
function isPalindrome(string) {
  let reverse = string.split("").reverse().join("");
  return reverse === string ? true : false;
}
let text = "racecar";
console.log(isPalindrome(text));

// 2.Return a sum of all parameters entered regardless of the amount of numbers - NO ARRAYS
function addAll(...nums) {
  return nums.reduce((prev, current) => {
    return prev + current;
  });
}

console.log(addAll(5, 2));
console.log(addAll(5, 5, 2));
console.log(addAll(5, 2, 5, 8));
