const sumArr = [0, 0];

const EvenOdd = (arr) => {
  arr.map((number) => {
    if (number % 2 == 0) {
      sumArr[0] += number;
    } else {
      sumArr[1] += number;
    }
    return sumArr;
  });
};

const numbers = [10, 50, 20, 20, 5, 7, 1];

EvenOdd(numbers);

console.log(sumArr);
