const stringChallenge = (str) => {
  str = str.split('');
  let temp = str.map((char) => {
    if (char.charCodeAt() >= 65 && char.charCodeAt() <= 90) {
      return char.toLowerCase();
    } else if (char.charCodeAt() >= 97 && char.charCodeAt() < 122) {
      return char.toUpperCase();
    } else {
      return char;
    }
  });
  temp = temp.join('');
  temp = temp.split(' ');
  temp = temp.map((word) => swap(word));
  temp = temp.join(' ');
  return temp;
};

const swap = (strArray) => {
  strArray = strArray.split('');
  for (let i = 0; i < strArray.length; i++) {
    if (isNaN(strArray[i + 1]))
      if (strArray[i].charCodeAt() >= 48 && strArray[i].charCodeAt() <= 57) {
        for (let j = i + 2; j < strArray.length; j++) {
          if (
            strArray[j].charCodeAt() >= 48 &&
            strArray[j].charCodeAt() <= 57
          ) {
            let temp = strArray[j];
            strArray[j] = strArray[i];
            strArray[i] = temp;
          }
        }
      }
  }
  strArray = strArray.join('');
  return strArray;
};

console.log(stringChallenge('2s 6 -5lol4 654665'));
