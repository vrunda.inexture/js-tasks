// 4).Convert below ES5 code to ES6
function isGreater(a, b) {
  return new Promise((resolve, reject) => {
    var greater = false;
    if (a > b) {
      resolve(true);
    }
    reject(greater);
  });
}

isGreater(8, 5)
  .then((result) => {
    console.log('greater');
  })
  .catch((result) => {
    console.log('smaller');
  });

// 6).write promise function call after 3 second and return 'hello world' in resolve
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Hello World afer 3sec');
  }, 3000);
});

promise
  .then((message) => {
    console.log(message);
  })
  .catch((err) => {
    console.log(err);
  });

// 7).write callback function print 'Hello world!' after 2 second
function hello() {
  console.log('Hello world');
}
setTimeout(hello, 2000);
